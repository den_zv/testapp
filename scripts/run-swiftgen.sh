#!/bin/sh
#
$PODS_ROOT/SwiftGen/bin/swiftgen xcassets "$PROJECT_DIR/$PROJECT_NAME/Resources/Assets.xcassets" -t swift4 --output "$PROJECT_DIR/$PROJECT_NAME/Common/Extensions/SwiftGen/Assets+SwiftGen.swift"

$PODS_ROOT/SwiftGen/bin/swiftgen strings "$PROJECT_DIR/$PROJECT_NAME/Resources/en.lproj/Localizable.strings" -t structured-swift4 --output "$PROJECT_DIR/$PROJECT_NAME/Common/Extensions/SwiftGen/Localization+SwiftGen.swift"

$PODS_ROOT/SwiftGen/bin/swiftgen ib "$PROJECT_DIR/$PROJECT_NAME/Resources/Storyboards" -t scenes-swift4 --output "$PROJECT_DIR/$PROJECT_NAME/Common/Extensions/SwiftGen/Storyboards+SwiftGen.swift"

# $PODS_ROOT/SwiftGen/bin/swiftgen coredata "$PROJECT_DIR/Core/Source/Database/SPACCECoreDataModel.xcdatamodeld" -t swift4 --output "$PROJECT_DIR/Core/Source/Database/Extensions/SwiftGen/NSManagedObject+SwiftGen.swift"

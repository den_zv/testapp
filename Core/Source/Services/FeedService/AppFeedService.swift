//
//  AppFeedService.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation
import FeedKit

public enum FeedServiceError: Error {
    case unsupportedURL
}

final public class AppFeedService: FeedService {
    
    private let keyValueStorage: KeyValueStorage
    
    private var handler: ((Result<[FeedElement], Error>) -> Void)?
    private var parser: FeedParser!
    
    public init(keyValueStorage: KeyValueStorage) {
        self.keyValueStorage = keyValueStorage
    }
    
    public func refetcnElements() {
        guard
            let string = keyValueStorage.object(forKey: Constants.UserDefaultKeys.AppSettings.feedURLString) as? String,
            let URL = URL(string: string)
            else {
                self.handler?(.failure(FeedServiceError.unsupportedURL))
                return
        }
        
        parser = FeedParser(URL: URL)
        
        parser.parseAsync(queue: DispatchQueue.global(qos: .userInitiated)) { (result) in
            // TODO: save data into DB here
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                switch result {
                case .success(let feed):
                    if case .rss(let feed) = feed,
                        let items = feed.items {
                        self.handler?(.success(items))
                    } else {
                        self.handler?(.failure(FeedServiceError.unsupportedURL))
                    }
                    
                case .failure(let error):
                    self.handler?(.failure(error))
                }
            }
        }
    }
    
    public func observeCachedElements(_ handler: @escaping ((Result<[FeedElement], Error>) -> Void)) -> Discardable {
        self.handler = handler
        // TODO: there should be observation mechanism (e. g. fetched results controller)
        // with ability to discard observing
        handler(.success([]))
        return MockDiscardable()
    }
    
}

extension RSSFeedItem: FeedElement {
    
    public var id: String? {
        return guid?.value
    }
    
    public var summary: String? {
        return description
    }
    
    public var pictureURLString: String? {
        return enclosure?.attributes?.url
    }
    
    public var postURLString: String? {
        return link
    }
    
    public var postedAt: Date? {
        return pubDate
    }
    
}

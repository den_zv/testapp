//
//  MockFeedService.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

// TODO: make it private when observation stuff is ready
class MockDiscardable: Discardable {
    
    func discard() {}
    
}

private struct MockFeedElement: FeedElement {
    
    public let id: String?
    public let title: String?
    public let summary: String?
    public let pictureURLString: String?
    public let postURLString: String?
    public let postedAt: Date?
    
}

final public class MockFeedService: FeedService {
    
    private var handler: ((Result<[FeedElement], Error>) -> Void)?
    
    // MARK: - lifecycle
    
    public init() {}
    
    public func refetcnElements() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
            guard let self = self else { return }
            self.handler?(.success(self.mockElements))
        }
    }
    
    public func observeCachedElements(_ handler: @escaping ((Result<[FeedElement], Error>) -> Void)) -> Discardable {
        self.handler = handler
        
        let elements = Array(mockElements[...1])
        handler(.success(elements))
        
        return MockDiscardable()
    }
    
}

private extension MockFeedService {
    
    //swiftlint:disable line_length
    var mockElements: [FeedElement] {
        return [
        MockFeedElement(
            id: "1",
            title: "First element",
            summary: "Just a first element",
            pictureURLString: "https://cdn.pixabay.com/photo/2014/11/30/14/11/kitty-551554_1280.jpg",
            postURLString: "https://cdn.pixabay.com/photo/2014/11/30/14/11/kitty-551554_1280.jpg",
            postedAt: Date()
        ),
        MockFeedElement(
            id: "2",
            title: "Second element",
            summary: "Just a second element Just a second element Just a second element Just a second element Just a second element Just a second element Just a second element Just a second element Just a second element Just a second element",
            pictureURLString: "https://cdn.pixabay.com/photo/2017/11/09/21/41/cat-2934720_1280.jpg",
            postURLString: "https://cdn.pixabay.com/photo/2017/11/09/21/41/cat-2934720_1280.jpg",
            postedAt: Date().addingTimeInterval(-11)
        ),
        MockFeedElement(
            id: "3",
            title: "Third element Third element Third element Third element Third element Third element Third element Third element Third element Third element",
            summary: "Just a third element",
            pictureURLString: "https://cdn.pixabay.com/photo/2017/07/25/01/22/cat-2536662_1280.jpg",
            postURLString: "https://cdn.pixabay.com/photo/2017/07/25/01/22/cat-2536662_1280.jpg",
            postedAt: Date().addingTimeInterval(-27)
        )
        ]
    }
    
}

//
//  FeedService.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

// TODO: move this to Common folder when observaiton stuff is ready
public protocol Discardable {
    
    func discard()
    
}

public protocol FeedService {
    
    func refetcnElements()
    func observeCachedElements(_ handler: @escaping ((Result<[FeedElement], Error>) -> Void)) -> Discardable
    
}

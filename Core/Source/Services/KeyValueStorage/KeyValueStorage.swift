//
//  KeyValueStorage.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

public protocol KeyValueStorage {
    
    func set(_ value: Any?, forKey key: String)
    func object(forKey key: String) -> Any?
    func saveChanges()
    
}

extension UserDefaults: KeyValueStorage {
    
    public func saveChanges() {
        synchronize()
    }
    
}

//
//  FeedElement.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

public protocol FeedElement {
    
    var id: String? { get }
    var title: String? { get }
    var summary: String? { get }
    var pictureURLString: String? { get }
    var postURLString: String? { get }
    var postedAt: Date? { get }
    
}

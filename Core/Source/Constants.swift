//
//  Constants.swift
//  Core
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

//swiftlint:disable nesting
enum Constants {
    
    enum UserDefaultKeys {
        
        enum AppSettings {
            
            static let feedURLString = "app_feed_url"
            
        }
        
    }
    
}

//
//  FeedFlowCoordinator.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit
import Core

final class FeedFlowCoordinator: FlowCoordinator {
    
    weak var containerViewController: UIViewController?
    
    // MARK: - lifecycle
    
    @discardableResult
    func createFlow() -> UIViewController {
        // TODO: remove mock from here
//        let model = FeedModel(feedService: MockFeedService(), output: self)
        let model = FeedModel(feedService: AppFeedService(keyValueStorage: UserDefaults.standard), output: self)
        let viewModel = FeedViewModel(model: model)
        let viewController = StoryboardScene.Feed.feed.instantiate()
        viewController.setup(with: viewModel)
        
        return viewController
    }
    
    // MARK: - private
    
    private func presentDetails(_ element: FeedElement) {
        let model = FeedDetailsModel(element: element)
        let viewModel = FeedDetailsViewModel(model: model)
        let viewController = StoryboardScene.Feed.feedDetails.instantiate()
        viewController.setup(with: viewModel)
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension FeedFlowCoordinator: FeedModelOutput {
    
    func feedElementSelected(_ element: FeedElement) {
        presentDetails(element)
    }
    
}

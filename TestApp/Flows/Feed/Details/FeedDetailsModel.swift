//
//  FeedDetailsModel.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Core

final class FeedDetailsModel {
    
    let element: FeedElement
    
    init(element: FeedElement) {
        self.element = element
    }
    
}

//
//  FeedDetailsViewModel.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

final class FeedDetailsViewModel {
    
    var pictureURL: URL? { return model.element.pictureURLString.flatMap(URL.init) }
    var dateText: String? {
        return model.element.postedAt.flatMap { UIDateFormatters.feedElementFormatter.string(from: $0) }
    }
    var URLText: String? { return model.element.postURLString }
    var elementURL: URL? { return model.element.postURLString.flatMap(URL.init) }
    var title: String? { return model.element.title }
    var summary: String? { return model.element.summary }
    
    private let model: FeedDetailsModel
    
    init(model: FeedDetailsModel) {
        self.model = model
    }
    
}

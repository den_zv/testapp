//
//  FeedDetailsViewController.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit
import Kingfisher
import RxSwift

final class FeedDetailsViewController: UIViewController {
    
    @IBOutlet private weak var pictureImageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var URLLabel: UILabel!
    @IBOutlet private weak var openInBrowserButton: UIButton!
    @IBOutlet private weak var titleTextLabel: UILabel!
    @IBOutlet private weak var summaryTextLabel: UILabel!
    
    private var viewModel: FeedDetailsViewModel!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTexts()
        setupContent()
        setupBindings()
    }
    
    func setup(with viewModel: FeedDetailsViewModel) {
        self.viewModel = viewModel
    }
    
    // MARK: - private
    
    private func setupTexts() {
        title = L10n.FeedDetails.title
        openInBrowserButton.setTitle(L10n.FeedDetails.openInBrowser, for: .normal)
    }
    
    private func setupContent() {
        pictureImageView.kf.setImage(with: viewModel.pictureURL)
        dateLabel.text = viewModel.dateText
        URLLabel.text = viewModel.URLText
        openInBrowserButton.isHidden = viewModel.elementURL == nil
        titleTextLabel.text = viewModel.title
        summaryTextLabel.text = viewModel.summary
    }
    
    private func setupBindings() {
        openInBrowserButton.rx
            .tap
            .bind { [weak self] in
                guard let self = self, let URL = self.viewModel.elementURL else { return }
                if UIApplication.shared.canOpenURL(URL) {
                    UIApplication.shared.open(URL, options: [:], completionHandler: nil)
                }
            }
            .disposed(by: disposeBag)
    }
    
}

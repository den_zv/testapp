//
//  FeedModel.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Core
import RxCocoa

protocol FeedModelOutput: class {
    func feedElementSelected(_ element: FeedElement)
}

final class FeedModel {
    
    var elementsUpdated: Driver<[FeedElement]> { return elements.asDriver() }
    var errorAppeared: Signal<Error> { return errorAction.asSignal() }
    
    private let feedService: FeedService
    private weak var output: FeedModelOutput?
    
    private let elements = BehaviorRelay<[FeedElement]>(value: [])
    private let errorAction = PublishRelay<Error>()
    
    private var elementsDiscardable: Discardable?
    
    // MARK: - lifecycle
    
    deinit {
        elementsDiscardable?.discard()
    }
    
    init(feedService: FeedService, output: FeedModelOutput?) {
        self.feedService = feedService
        self.output = output
        
        observeElements()
    }
    
    func refetchElements() {
        feedService.refetcnElements()
    }
    
    func itemSelected(at index: Int) {
        output?.feedElementSelected(elements.value[index])
    }
    
    // MARK: - private
    
    private func observeElements() {
        elementsDiscardable?.discard()
        elementsDiscardable = feedService.observeCachedElements { [unowned self] result in
            switch result {
            case .success(let elements):
                self.elements.accept(elements)
            case .failure(let error):
                self.errorAction.accept(error)
            }
        }
    }
    
}

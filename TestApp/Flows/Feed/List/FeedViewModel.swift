//
//  FeedViewModel.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Core
import RxCocoa

final class FeedViewModel {
    
    var displayablesUpdated: Driver<[FeedCellDisplayable]> {
        return model.elementsUpdated.map { $0.map(FeedCellDisplayable.init) }
    }
    
    var errorAppeared: Signal<String> { return model.errorAppeared.map { $0.localizedDescription } }
    
    private let model: FeedModel
    
    init(model: FeedModel) {
        self.model = model
    }
    
    func viewLoaded() {
        model.refetchElements()
    }
    
    func refresh() {
        model.refetchElements()
    }
    
    func itemSelected(at index: Int) {
        model.itemSelected(at: index)
    }
    
}

private extension FeedCellDisplayable {
    
    init(element: FeedElement) {
        self = FeedCellDisplayable(
            title: element.title,
            summary: element.summary,
            pictureURL: element.pictureURLString.flatMap(URL.init)
        )
    }
    
}

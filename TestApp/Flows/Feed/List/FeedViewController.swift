//
//  FeedViewController.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class FeedViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var refreshButton: UIButton!
    
    private var viewModel: FeedViewModel!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTitle()
        setupButtons()
        setupBindings()
        
        viewModel.viewLoaded()
    }
    
    func setup(with viewModel: FeedViewModel) {
        self.viewModel = viewModel
    }
    
    // MARK: - private
    
    private func setupTitle() {
        title = L10n.Feed.title
    }
    
    private func setupButtons() {
        refreshButton.setTitle(L10n.Feed.Button.refresh, for: .normal)
    }
    
    private func setupBindings() {
        viewModel
            .displayablesUpdated
            .drive(tableView.rx.items(cellIdentifier: "FeedCell")) { (_, model, cell: FeedCell) in
                cell.setup(with: model)
            }
            .disposed(by: disposeBag)
        viewModel
            .errorAppeared
            .emit(onNext: { [weak self] text in
                self?.handleError(text)
            })
            .disposed(by: disposeBag)
        
        refreshButton.rx
            .tap
            .bind(onNext: viewModel.refresh)
            .disposed(by: disposeBag)
        tableView.rx
            .itemSelected.map { $0.row }
            .bind(onNext: viewModel.itemSelected)
            .disposed(by: disposeBag)
    }
    
    private func handleError(_ text: String) {
        let alertController = UIAlertController(title: L10n.Errors.Alert.title, message: text, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(
            title: L10n.Common.ok,
            style: .default,
            handler: nil
        ))
        present(alertController, animated: true, completion: nil)
    }
    
}

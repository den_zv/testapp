//
//  FeedCell.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit
import Kingfisher

struct FeedCellDisplayable {
    
    public let title: String?
    public let summary: String?
    public let pictureURL: URL?
    
}

final class FeedCell: UITableViewCell {
    
    @IBOutlet private weak var pictureImageView: UIImageView!
    @IBOutlet private weak var titleTextLabel: UILabel!
    @IBOutlet private weak var summaryTextLabel: UILabel!
    
    // MARK: - lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        pictureImageView.kf.cancelDownloadTask()
        pictureImageView.image = nil
    }
    
    func setup(with displayable: FeedCellDisplayable) {
        if let URL = displayable.pictureURL {
            pictureImageView.kf.setImage(with: URL)
        }
        titleTextLabel.text = displayable.title
        summaryTextLabel.text = displayable.summary
    }
    
}

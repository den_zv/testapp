//
//  ApplicationFlowCoordinator.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit

final class ApplicationFlowCoordinator {
    
    private let window: UIWindow
    
    private var feedFlowCoordinator: FeedFlowCoordinator!
    
    // MARK: init
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func execute() {
        feedFlowCoordinator = FeedFlowCoordinator()
        
        let controller = feedFlowCoordinator.createFlow()
        let navigationController = UINavigationController(rootViewController: controller)
        
        feedFlowCoordinator.containerViewController = navigationController
        setWindowRootViewController(with: navigationController)
    }
    
    // MARK: Helpers
    
    private func setWindowRootViewController(with viewController: UIViewController) {
        window.rootViewController = viewController
        window.makeKeyAndVisible()
    }
    
}

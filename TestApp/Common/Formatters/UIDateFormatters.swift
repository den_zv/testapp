//
//  UIDateFormatters.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import Foundation

struct UIDateFormatters {
    
    static let feedElementFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter
    }()
    
}

// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum Common {
    /// OK
    internal static let ok = L10n.tr("Localizable", "common.ok")
  }

  internal enum Errors {
    internal enum Alert {
      /// Error
      internal static let title = L10n.tr("Localizable", "errors.alert.title")
    }
  }

  internal enum Feed {
    /// Feed
    internal static let title = L10n.tr("Localizable", "feed.title")
    internal enum Button {
      /// Refresh
      internal static let refresh = L10n.tr("Localizable", "feed.button.refresh")
      /// Settings
      internal static let settings = L10n.tr("Localizable", "feed.button.settings")
    }
  }

  internal enum FeedDetails {
    /// Open in browser
    internal static let openInBrowser = L10n.tr("Localizable", "feed_details.open_in_browser")
    /// Feed details
    internal static let title = L10n.tr("Localizable", "feed_details.title")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}

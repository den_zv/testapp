//
//  FlowCoordinator.swift
//  TestApp
//
//  Created by Denis on 11/4/19.
//  Copyright © 2019 --. All rights reserved.
//

import UIKit

protocol FlowCoordinator: class {
    
    var containerViewController: UIViewController? { get set }
    @discardableResult
    func createFlow() -> UIViewController
    
}

extension FlowCoordinator {
    
    var navigationController: UINavigationController? {
        return containerViewController as? UINavigationController
    }
    
}
